pragma solidity 0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TaBaToken is ERC20 {
  constructor() ERC20("TaBa", "TBT") {
    _mint(msg.sender, 12000);
  }
}
