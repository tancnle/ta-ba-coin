import React, { Component } from "react";
import {
  AccountData,
  ContractData,
  ContractForm
} from "drizzle-react-components";
import logo from "../../coin.png";

class Home extends Component {
  render() {
    return (
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1 header">
            <img src={logo} height="100px" alt="coin-logo" />
            <h1>TaBaCoin</h1>
          </div>

          <div className="pure-u-1-1">
            <h2>Active Account</h2>
            <AccountData accountIndex="0" units="ether" precision="3" />
          </div>

          <div className="pure-u-1-1">
            <h2>TaBaCoin</h2>
            <p>
              <strong>Total Supply</strong>:{" "}
              <ContractData
                contract="TaBaToken"
                method="totalSupply"
                methodArgs={[{ from: this.props.accounts[0] }]}
              />{" "}
              <ContractData
                contract="TaBaToken"
                method="symbol"
                hideIndicator
              />
            </p>
            <p>
              <strong>My Balance</strong>:{" "}
              <ContractData
                contract="TaBaToken"
                method="balanceOf"
                methodArgs={[this.props.accounts[0]]}
              />
            </p>
            <h3>Send Tokens</h3>
            <ContractForm
              contract="TaBaToken"
              method="transfer"
              labels={["To Address", "Amount to Send"]}
            />
          </div>
        </div>
      </main>
    );
  }
}

export default Home;
