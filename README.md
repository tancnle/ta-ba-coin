# TaBaCoin

Experimenting with create a coin with Solidity and Truffle.
Contracts are based on [OpenZeppelin framework](https://docs.openzeppelin.com/openzeppelin/).

## Requirements

- [Geth](https://geth.ethereum.org/)
- [Truffle](http://truffleframework.com/)
- [Ganache CLI](https://github.com/trufflesuite/ganache-cli)

## Getting Started

- Run and RPC client with Ganache CLI. This will create 10 test accounts with corresponding private keys.
  Make sure you save those for later references.

```sh
# Increase block time
ganache-cli -b 3
```

- Leave the above running, on another session run migrations to deploy all contracts

```sh
truffle migrate --reset
```

- Interact with console to play with contracts

```sh
truffle console
```

- Install Drizzle app dependencies

```sh
yarn install
```

- Start static web server on port 3000:

```sh
yarn run start
```

- Follow the instruction to hook up [MetaMask to Truffle](http://truffleframework.com/docs/advanced/truffle-with-metamask).
  You can use the above generated private keys from `ganache-cli` to hook these test accounts for MetaMask.

- Navigate to http://localhost:3000 and start to transfer coin between accounts.

## References

- [The Hitchhikers Guide to Smart Contracts](https://blog.zeppelin.solutions/the-hitchhikers-guide-to-smart-contracts-in-ethereum-848f08001f05)
- [OpenZeppelin](https://github.com/OpenZeppelin/openzeppelin-solidity)
- [Learn Solidity](https://learnxinyminutes.com/docs/solidity/)
